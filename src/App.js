import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';
import Accordion from './components/Accordion';

const App = () => {
  const api = 'https://api.myjson.com/bins/jw3rg';
  const [data, setData] = useState([]);
  const [loading, setLoading ] = useState(true);
 const getData = (url) => {
  return axios
    .get(url)
    .then(res => {
        return res.data;
    })
    .catch(error => {
        return ('Error Fetching and parsing data', error);
    });
 }

 useEffect(()=> {
   const get = async() => {
    const res = await getData(api);
    setData(res);
    setLoading(false);
   }
  get();
 }, [loading]);
 if(loading) {
  return 'Loading....';
 }
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
      {data.faqs.map(({ question, answer, id }) => (
        <Accordion
        key= {id} 
        que= {question}
        ans= {answer}
        />
      ))}
    </div>
  );
}

export default App;


